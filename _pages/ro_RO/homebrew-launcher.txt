---
title: "Homebrew Launcher"
---

{% include toc title="Cuprins" %}

### Lectură obligatorie

Homebrew Launcher este o aplicație homebrew care afișează o listă și permite pornirea altor aplicații homebrew de pe cardul SD.

Pentru a-l porni vom folosi browserul de internet incorporat în Wii U, așa că Wii U-ul va avea nevoie de acces la Internet.

### Instrucțiuni

#### Secțiunea I - Pregătind browserul

1. Porniți browserul pe consolă
1. Mergeți la setările de browser și selectați "Reset Save Data"
  + Aceasta va preveni probleme cu exploit-ul
  + Aceasta va șterge de asemenea toate datele care au legătură cu setările, istoricul de navigare și favoriți
1. Întoarceți-vă la browser

#### Secțiunea II - Homebrew Launcher-ul

1. În cazul în care consola este pe versiuni de firmware 5.5.2 sau 5.5.3, deschideți perdelele de browser
  + Aceasta îmbunătățește rata de succes a exploit-ului de browser 5.5.2 / 5.5.3
1. Mergeți la `usploit.hacks.guide`
  + Ar fi mai convenavil să adăugați această adreasă la favoriți și să economisiți timpul de a-l mai scrie pe viitor
1. Selectați "Run Homebrew Launcher" pentru prina încercare de pornire de exploit
  + S-ar putea să fie nevoie să încercați de mai multe ori
  + Dacă se blochează, închideți consola forțat ținând apăsat butonul POWER și încercați din nou
1. Consola dumneavoastră ar trebui să încarce meniul homebrew

___
### Metode

___

#### Mocha CFW

Această metodă are nevoie să vă reporniți exploit-ul de web folosit *după fiecare repornire*.

### Continuați la [Mocha CFW](mocha-cfw)
{: .notice--primary}

___

#### Haxchi

Această metodă folosește un joc de consolă virtuală DS ieftină și vulnerabilă pentru a porni Custom Firmware automatic la pornirea consolei.

### Continuați la [Haxchi](haxchi)
{: .notice--primary}

___
